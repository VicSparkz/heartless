﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents
{
    public static Action<float> updateStaminaBar;
    public static Action<int> updateScore;
    public static Action onGameOver;
}
