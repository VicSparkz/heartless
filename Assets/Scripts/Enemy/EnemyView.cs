﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyView : MonoBehaviour
{
    public Animator body;
    public Image staminaBar;

    private float stamina = 100;

    private new Rigidbody2D rigidbody2D;
    private Collider2D collider2D;

    private GameObject player;
    private float attackCounter = 0.0f;

	private void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        collider2D = GetComponent<Collider2D>();

        player = GameObject.FindGameObjectWithTag(UnityConstants.Tags.Player);
	}

    private void OnEnable()
    {
        body.GetComponent<AnimatorWatcher>().animationComplete += AnimationComplete;
    }

    private void OnDisable()
    {
        body.GetComponent<AnimatorWatcher>().animationComplete -= AnimationComplete;
    }

    private void Update()
    {
        stamina = Mathf.Clamp(stamina + 0.25f, 0, 100);
        staminaBar.fillAmount = stamina / 100;

        if(Vector2.Distance(transform.position, player.transform.position) < 10)
        {
            attackCounter += Time.deltaTime;

            if(attackCounter > 2.5f)
            {
                rigidbody2D.AddForce(new Vector2(player.transform.position.x - transform.position.x, player.transform.position.y - transform.position.y) * 500);
                attackCounter = 0.0f;

                body.Play("Attack");

                //transform.position = Vector2.MoveTowards(transform.position, player.transform.position, 0.05f);
            }
            else if(attackCounter > 2.0f)
            {
                body.Play("PreAttack");
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == UnityConstants.Tags.PlayerAttack)
        {
            rigidbody2D.AddForce(new Vector2(transform.position.x - collision.transform.position.x, transform.position.y - collision.transform.position.y) * 500);

            stamina -= 40.0f;

            if(stamina <= 0)
            {
                GameEvents.updateScore(50);

                Destroy(gameObject);
            }
        }
    }

    private void AnimationComplete(int stateHash)
    {
        if (Animator.StringToHash("Attack") == stateHash)
        {
            body.Play("Idle");
        }
    }
}
