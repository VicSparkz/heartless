﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemyPrefab;

    public List<Transform> spawnPoints;

    private float spawnTimer = 7.0f;
    private float currentSpawnTime = 10.0f;

	private void Start ()
    {
		
	}
	
	private void Update ()
    {
        spawnTimer += Time.deltaTime;

        if(spawnTimer > currentSpawnTime)
        {
            spawnTimer = 0;

            currentSpawnTime = Mathf.Max(currentSpawnTime - 0.25f, 0);

            GameObject enemy = Instantiate(enemyPrefab) as GameObject;
            enemy.transform.position = spawnPoints[Random.Range(0, spawnPoints.Count)].position;
        }
	}
}
