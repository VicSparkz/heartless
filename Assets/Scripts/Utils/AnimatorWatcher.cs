﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorWatcher : MonoBehaviour
{
    public Action<int> animationComplete;

    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Update ()
    {
        if(animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
        {
            if(animationComplete != null)
            {
                animationComplete(animator.GetCurrentAnimatorStateInfo(0).shortNameHash);
            }
        }
	}
}
