﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour
{
    public Text scoreText;
    public GameObject gameOverScreen;
    public Text finalScoreText;

    private int currentScore = 0;

    private void OnEnable()
    {
        GameEvents.updateScore += UpdateScore;
        GameEvents.onGameOver += OnGameOver;
    }

    private void OnDisable()
    {
        GameEvents.updateScore -= UpdateScore;
        GameEvents.onGameOver -= OnGameOver;
    }

    private void UpdateScore(int value)
    {
        currentScore += value;

        scoreText.text = "score: " + currentScore.ToString("0000");
    }

    private void OnGameOver()
    {
        gameOverScreen.SetActive(true);

        finalScoreText.text = "Final Score - " + currentScore.ToString("0000");
    }
}
