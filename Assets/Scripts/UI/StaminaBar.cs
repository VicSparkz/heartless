﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaBar : MonoBehaviour {

    private Image image;

	private void Start()
    {
        image = GetComponent<Image>();
	}
	
	private void OnEnable()
    {
        GameEvents.updateStaminaBar += UpdateStaminaBar;
	}

    private void OnDisable()
    {
        GameEvents.updateStaminaBar -= UpdateStaminaBar;
    }

    private void UpdateStaminaBar(float value)
    {
        image.fillAmount = value;
    }
}
