﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScreenView : MonoBehaviour
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        InputDevice inputDevice = InputManager.ActiveDevice;

        if(inputDevice.MenuWasPressed)
        {
            SceneManager.LoadScene(UnityConstants.Scenes.Level_1);
        }
    }
}
