﻿using InControl;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets._2D;

public class PlayerView : MonoBehaviour
{
    public Animator body;
    public Camera2DFollow camera;

    private bool dead = false;

    private new Rigidbody2D rigidbody2D;

    private float stamina = 100;

    private Vector3 velocity = new Vector3();

    private float movementSpeed = 7.5f;
    private float dashSpeed = 35.0f;
    private float dashCost = 10.0f;
    private float dashDelay = 1.0f;
    private float dashLength = 0.1f;
    private float dashDelayCounter = 1.0f;

    private bool isAttacking = false;

    private bool willAttack = false;
    private float attackCost = 10.0f;
    private float comboTimer = 0;
    private int comboCount = 0;
    private float[] comboTimes = {0.0f, 0.25f, 0.25f, 1.0f };
    private string[] combo = { "Attack", "Attack2", "Attack" };

    private void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void OnEnable()
    {
        body.GetComponent<AnimatorWatcher>().animationComplete += AnimationComplete;
    }

    private void OnDisable()
    {
        body.GetComponent<AnimatorWatcher>().animationComplete -= AnimationComplete;
    }

    private void Update ()
    {
        InputDevice inputDevice = InputManager.ActiveDevice;

        if (dead)
        {
            if(inputDevice.MenuWasPressed)
            {
                SceneManager.LoadScene(UnityConstants.Scenes.Title);
            }
        }

        stamina = Mathf.Clamp(stamina + 0.5f, 0, 100);
        GameEvents.updateStaminaBar(stamina/100);

        dashDelayCounter += Time.deltaTime;

        if(!isAttacking)
        {
            float xMovement = movementSpeed * Time.deltaTime * inputDevice.LeftStickX;
            float yMovement = movementSpeed * Time.deltaTime * inputDevice.LeftStickY;

            float xRightMovement = inputDevice.RightStickX;
            float yRightMovement = inputDevice.RightStickY;

            if((Mathf.Abs(xRightMovement) > 0.5f || Mathf.Abs(yRightMovement) > 0.5f) && dashDelayCounter > dashDelay)
            {
                //transform.position += new Vector3(xRightMovement * movementSpeed * Time.deltaTime, yRightMovement * movementSpeed * Time.deltaTime, transform.position.z);
                velocity.x = xRightMovement * dashSpeed * Time.deltaTime;
                velocity.y = yRightMovement * dashSpeed * Time.deltaTime;

                dashDelayCounter = 0;
            }
            else if(dashDelayCounter > dashLength)
            {
                //transform.position += new Vector3(xMovement, yMovement, transform.position.z);
                velocity.x = xMovement;
                velocity.y = yMovement;

                if (Mathf.Abs(xMovement) > 0.01f || Mathf.Abs(yMovement) > 0.01f)
                {
                    if(Mathf.Abs(xMovement) > Mathf.Abs(yMovement))
                    {
                        body.SetFloat("directionX", xMovement > 0 ? 1 : -1);
                        body.SetFloat("directionY", 0);
                    }
                    else
                    {
                        body.SetFloat("directionX", 0);
                        body.SetFloat("directionY", yMovement > 0 ? 1 : -1);
                    }

                    body.Play("Walk");
                }
                else
                {
                    body.Play("Idle");
                }
            }

            transform.position += velocity;
        }

        if(inputDevice.Action1.WasPressed && stamina > attackCost)
        {
            willAttack = true;
        }
        
        if(comboCount > 0)
        {
            comboTimer += Time.deltaTime;

            if(comboTimer >= 1.5f)
            {
                comboCount = 0;
                comboTimer = 0;
                willAttack = false;
            }
        }

        if(comboCount < combo.Length && willAttack && (comboTimer > comboTimes[comboCount] || comboCount == 0))
        {
            body.Play(combo[comboCount]);
            isAttacking = true;
            stamina -= attackCost;
            willAttack = false;
            comboTimer = 0;

            comboCount++;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == UnityConstants.Tags.Enemy)
        {
            stamina -= 20.0f;

            if (stamina <= 0)
            {
                GameEvents.onGameOver();
                camera.enabled = false;
                transform.position = Vector3.right * 10000;
                dead = true;
            }
        }
    }

    private void AnimationComplete(int stateHash)
    {
        for(int i = 0; i < combo.Length; i++)
        {
            if(Animator.StringToHash(combo[i]) == stateHash)
            {
                body.Play("Idle");
                isAttacking = false;
            }
        }
    }
}
